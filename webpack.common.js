const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

module.exports = {
	target: 'node',
	entry: `./src/server.js`,
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: 'server.js',
	},
	stats: {
		colors: true,
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/, /test/],
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	plugins: [new CleanWebpackPlugin('dist', {})],
};

# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.0"></a>
# [1.1.0](https://bitbucket.org/masjiene/hardware-server/compare/v1.0.1...v1.1.0) (2019-02-07)


### Bug Fixes

* **hardware:** added stuff ([e9f1b5c](https://bitbucket.org/masjiene/hardware-server/commits/e9f1b5c))
* **hardware:** deployed ([ffdb035](https://bitbucket.org/masjiene/hardware-server/commits/ffdb035))


### Features

* **hardware server:** Added project setup and config, added mongoDB, added collectio.watch ([704c276](https://bitbucket.org/masjiene/hardware-server/commits/704c276))
* **hardware-server:** Added build capabilities ([c8e5c1c](https://bitbucket.org/masjiene/hardware-server/commits/c8e5c1c))
* **self:** reafactored resolvers ([2238620](https://bitbucket.org/masjiene/hardware-server/commits/2238620))
* **self:** Webpack config ([317f2c0](https://bitbucket.org/masjiene/hardware-server/commits/317f2c0))



<a name="1.0.1"></a>
## 1.0.1 (2018-12-16)


### Bug Fixes

* **hardware-server:** Trying to create dev evnvironment for rpi project ([4a0889f](https://bitbucket.org/masjiene/hardware-server/commits/4a0889f))

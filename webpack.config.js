const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

const appConfig = {
	target: 'node',
	mode: 'development',
	entry: {
		app: `${path.resolve(__dirname, 'src')}/index.js`,
	},
	output: {
		filename: 'server.js',
		path: path.resolve(__dirname, 'dist'),
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/, /test/],
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	plugins: [new CleanWebpackPlugin('dist', {})],
};
module.exports = [appConfig];

const deviceTransmitter = require('./device.transmitter');

module.exports = deviceModifier = device => {
	if (!device) {
		return null;
	}

	deviceTransmitter(device);
};

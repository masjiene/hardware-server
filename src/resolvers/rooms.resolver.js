const deviceModifier = require('./device.modifier');

module.exports = roomResolver = rooms => {
	if (!rooms) {
		return null;
	}

	rooms.forEach(room => {
		if (!room.devices) {
			return null;
		}

		room.devices.forEach(device => {
			deviceModifier(device);
		});
	});
};

const roomResolver = require('./rooms.resolver');
const houseResolver = require('./house.resolver');

module.exports = resolver = object => {
	if (!object) {
		return null;
	}

	roomResolver(object.rooms);
	houseResolver(object);
};

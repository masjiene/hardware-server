const deviceConfig = require('./device.conf');
const http = require('http');

module.exports = deviceTransmitter = device => {
	if (!device) {
		return null;
	}

	console.log(
		`API call to http://${device.address}"${device.port}/${device.command}`
	);

	// TODO: Finish http calls
	// http.get(
	// 	`http://${device.address}"${device.port}/${device.command}`,
	// 	res => {
	// 		res.setEncoding('utf8');
	// 		let body = JSON.stringify({ state: device.state });
	// 		res.on('data', data => {
	// 			console.log(data);
	// 		});
	// 		res.on('end', () => {
	// 			console.log('end');
	// 		});
	// 	}
	// );
};

module.exports = {
	types: {
		lock: {
			url: 'http://lock.com/api/',
		},
		window: {
			url: 'http://window.com/api/',
		},
		heating: {
			url: 'http://heating.com/api/',
		},
		light: {
			url: 'http://light.com/api/',
		},
		speaker: {
			url: 'http://speaker.com/api/',
		},
	},
};

const deviceModifier = require('./device.modifier');

module.exports = houseResolver = house => {
	if (!house.devices) {
		return null;
	}

	house.devices.forEach(device => {
		deviceModifier(device);
	});
};

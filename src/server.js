require('@babel/polyfill');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const resolver = require('./resolvers/index');

const app = express();
const router = express.Router();
const houseRoutes = require('./houseRoutes');

app.use(bodyParser.json());
app.use(
	bodyParser.urlencoded({
		extended: true,
	})
);
app.use(cors());
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content-Type, Accept'
	);
	next();
});

router.get('/', (req, res) => {
	res.redirect('index.html');
});

app.use('/api/', houseRoutes());

const pipeline = [
	{
		$project: { documentKey: false },
	},
];

MongoClient.connect(
	`mongodb+srv://${process.env.MONGO_USER}:${
		process.env.MONGO_PASSWORD
	}@swyserdb-jfjw9.mongodb.net?retryWrites=true`,
	{ useNewUrlParser: true }
)
	.then(client => {
		console.log('db connection successful!');
		const db = client.db(process.env.MONGO_DB);
		const collection = db.collection('houses');

		const changeStream = collection.watch(pipeline);
		changeStream.on('change', change => {
			resolver(change.fullDocument);
		});
	})
	.catch(err => {
		console.log('db connection failed!', err);
	});

app.listen(8084);
console.log('Server running on port 8084...');

exports = module.exports = app;

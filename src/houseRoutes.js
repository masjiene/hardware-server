const express = require('express');
// const Gpio = require('onoff').Gpio;

const routes = () => {
	// const led = new Gpio(14, 'out');
	const houseRoutes = express.Router();
	houseRoutes.get('/gpio14on', (req, res) => {
		// led.writeSync(1);
		res.status(200).json({ status: 'on' });
	});
	houseRoutes.get('/gpio14off', (req, res) => {
		// led.writeSync(0);
		res.status(200).json({ status: 'off' });
	});

	return houseRoutes;
};
module.exports = ('houseRoutes', routes);
